import requests
import csv
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup





constructeur = []
modele = []
kilometrage = []
transmission = []
carburant = []
annee = []
localisation = []
contact = []
prix = []
vendeur = []
i = 0

#creation de la methode permettant de recuperer les donnees d'une page du site coinafrique
def function(url):
    response = requests.get(url)
    if response.ok:
        try:

            soup = BeautifulSoup(response.text, "html.parser")
            result = soup.find('div', class_='adcard__listing')

            results = result.findAll('div')

            for result in result:
				
				#recuperation du prix 
                pri = result.find('p', class_='ad__card-price')
                prixx = pri.find('a')
                prix.append(pri.text.replace("CFA", ""))
                print("0-"+pri.text)

				#recuperation de la localisation  
                desc = result.find('p', class_='ad__card-description')
                local = result.find('p', class_='ad__card-location')
                localisation.append(local.text.replace('location_on', ""))
                print("1-"+local.text)

				#recuperation de l'annee de la voiture
                taille = len(desc.text)
                annee.append(desc.text[taille-4:taille])
                print("2-"+desc.text[taille-4:taille])

                a = result.find('a')

				#nous entrons dans le lien detail correspondant a une offre
                lien = "https://bf.coinafrique.com"+a['href']

				#preparation de la page pour qu'elle puise etre exploré 
                details = requests.get(lien)
                detailss = BeautifulSoup(details.text, "html.parser")

				#recuperation du vendeur
                vend = detailss.find('p', class_="username")
                vendeur.append(vend.text)

                print(vend.text)

                result_detail = detailss.find(
                    'div', class_='details-characteristics')

                result_detail_li = result_detail.findAll('li')

				#recuperation du constructeur
                construc = result_detail_li[0].find('span', class_='qt')
                constructeur.append(construc.text)
                print("3-"+construc.text)

				#recuperation du modele
                mod = result_detail_li[1].find('span', class_='qt')
                modele.append(mod.text)
                print("4-"+mod.text)

				#recuperation du kilometrage
                kilo = result_detail_li[2].find('span', class_='qt')
                kilometrage.append(kilo.text)
                print("5-"+kilo.text)

				#recuperation de la transmission 
                transmi = result_detail_li[3].find('span', class_='qt')
                transmission.append(transmi.text)
                print("6-"+transmi.text)

				#recuperation du carburant
                carbur = result_detail_li[4].find('span', class_='qt')
                carburant.append(carbur.text)
                print("7-"+carbur.text)

				#recuperation du numero de telephone
                telephone = detailss.find(
                    'a', class_='bttn btn-contact btn-contact-tel gtm-mobile-appel')
                contact.append(telephone['href'].replace("tel:", ""))
                print("8-"+telephone['href'])

                i += 1
                print(i)
                print("_______________________________________________________")

        except Exception as e:
            print("erreurs")


#creation des liens a exploiter
urls = []
for i in range(1, 24):
    path = 'https://bf.coinafrique.com/categorie/voitures?page='
    urls.append(path+str(i))


#execution de la fonction pour tout les liens
for url in urls:
    function(url)
    print("test")

#creation d'un DataFrame grace a pandas 
carsBj = pd.DataFrame.from_dict({"Constructeur": constructeur, "Modele": modele, "Kilometrage": kilometrage, "Carburant": carburant,
                                 "Annee": annee, "Localisation": localisation, "Contact": contact, "Transmission": transmission, "prix": prix, "Vendeur": vendeur})
cars_prix = carsBj[carsBj['prix'] != 'Prix sur demande']
cars_sans_prix = carsBj[carsBj['prix'] == 'Prix sur demande']


#traitement des donees grace a pandas
prix = cars_prix['prix']
test = []
for p in prix:
    s = p.replace(" ", "")
    t = float(s)
    test.append(t)
cars_prix['prix'] = test
cars_prix.dropna(axis=0)

#enregistrement des donnees dans un fichier csv
cars_prix.to_csv('cars.csv', index=False, encoding='utf-8')
